<?php

namespace Drupal\Tests\address\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests address field validation.
 *
 * @group address
 */
class AddressValidationTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'address',
    'address_test',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->drupalCreateUser([
      'create address_test content',
    ]));
  }

  /**
   * Tests the default widget validation.
   *
   * @dataProvider defaultWidgetValidationDataProvider
   */
  public function testDefaultWidgetValidation($address, $validation_enabled, $error = NULL) {
    // Adjust the field settings.
    $field = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('node.address_test.field_address_test');
    $field->setSetting('validate_postal_code', $validation_enabled)->save();

    // Create a node with address field.
    $this->drupalGet('/node/add/address_test');

    // Fill the title to make sure we won't get validation errors on it.
    $this->getSession()->getPage()
      ->fillField('title[0][value]', 'Test address validation');

    // Select country to make other fields appear.
    $this->getSession()->getPage()
      ->selectFieldOption('field_address_test[0][address][country_code]', $address['country_code']);
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Fill the rest of the address fields.
    foreach ($address as $field => $value) {
      if ($field == 'country_code') {
        continue;
      }
      $this->getSession()->getPage()
        ->fillField("field_address_test[0][address][$field]", $value);
    }

    // Submit the form.
    $this->getSession()->getPage()->pressButton('Save');

    // Make sure error message is displayed if expected or not displayed if not.
    if ($error) {
      $this->assertSession()->statusMessageContains($error, 'error');
    }
    else {
      $this->assertSession()->statusMessageNotExists('error');
    }
  }

  /**
   * Provides data for ::testDefaultWidgetValidation().
   */
  public static function defaultWidgetValidationDataProvider(): array {
    $cases = [];

    // Valid address, validation enabled, no error message expected.
    $cases[] = [
      [
        'country_code' => 'GB',
        'given_name' => 'John',
        'family_name' => 'Smith',
        'address_line1' => '1 Main Street',
        'locality' => 'London',
        'postal_code' => 'SW1A 1AA',
      ],
      TRUE,
      NULL,
    ];
    // Valid address, validation disabled, no error message expected.
    $cases[] = [
      [
        'country_code' => 'GB',
        'given_name' => 'John',
        'family_name' => 'Smith',
        'address_line1' => '1 Main Street',
        'locality' => 'London',
        'postal_code' => 'SW1A 1AA',
      ],
      FALSE,
      NULL,
    ];
    // Non-valid address, validation enabled, error message expected.
    $cases[] = [
      [
        'country_code' => 'GB',
        'given_name' => 'John',
        'family_name' => 'Smith',
        'address_line1' => '1 Main Street',
        'locality' => 'London',
        'postal_code' => '111 SW1A 1AA',
      ],
      TRUE,
      'Postal code field is not in the right format.',
    ];
    // Non-valid address, validation disabled, no error message expected.
    $cases[] = [
      [
        'country_code' => 'GB',
        'given_name' => 'John',
        'family_name' => 'Smith',
        'address_line1' => '1 Main Street',
        'locality' => 'London',
        'postal_code' => '111 SW1A 1AA',
      ],
      FALSE,
      NULL,
    ];

    return $cases;
  }

}
