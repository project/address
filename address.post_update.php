<?php

/**
 * @file
 * Contains post-update hooks for Address.
 */

declare(strict_types=1);

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Add the "Wrapper type" setting to the default widget.
 */
function address_post_update_default_widget_wrapper(?array &$sandbox = NULL): void {
  \Drupal::classResolver(ConfigEntityUpdater::class)->update($sandbox, 'entity_form_display', function (EntityFormDisplayInterface $form_display): bool {
    $changed = FALSE;
    foreach ($form_display->getComponents() as $field => $component) {
      if (is_array($component)
        && array_key_exists('type', $component)
        && ($component['type'] === 'address_default')
        && !array_key_exists('wrapper_type', $component['settings'])) {
        $component['settings']['wrapper_type'] = 'details';
        $form_display->setComponent($field, $component);
        $changed = TRUE;
      }
    }

    return $changed;
  });
}

/**
 * Enable postal code validation for existing fields to maintain previous behavior.
 */
function address_post_update_enable_postal_code_validation(&$sandbox) {
  if (!isset($sandbox['fields'])) {
    $sandbox['fields'] = array_values(\Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->loadByProperties(['field_type' => 'address']));
    $sandbox['current'] = 0;
    $sandbox['count'] = count($sandbox['fields']);
  }

  for ($i = 0; $i < 20 && $sandbox['current'] < $sandbox['count']; $i++, $sandbox['current']++) {
    $field = $sandbox['fields'][$sandbox['current']]->toArray();
    $field['settings']['validate_postal_code'] = TRUE;

    $field = FieldConfig::create($field);
    $field->original = $sandbox['fields'][$sandbox['current']];
    $field->enforceIsNew(FALSE);
    $field->save();
  }
  $sandbox['#finished'] = !$sandbox['count'] ? 1 : $sandbox['current'] / $sandbox['count'];

  return t('Enabled postal code validation for address fields to maintain previous behavior.');
}
